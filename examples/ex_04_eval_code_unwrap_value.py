from _quickjs import JSException, Object
from thquickjs import QuickJS

## Errors handling
# create QuckJS object
qjs = QuickJS()

# example unparsable js code
code = '''unparsable js code'''

# standard way to handle exception using try and except blocks
try:
    qjs_func: Object = qjs.eval(code).unwrap()
except JSException as e:
    pass

# evaluation of js code and unwraps_value - returns exception as string
qjs_func = qjs.eval(code).unwrap_value()

# unwrap_or method sets default value in case of exception
qjs_func = qjs.eval(code).unwrap_or('default value in case of exception') # qjs_func in case of exception will have default value
