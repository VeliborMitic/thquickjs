import os
import json
import pytest

from quickjs import JSException, Object

from thresult import unwrap
from thquickjs.thquickjs import QuickJS


def test_eval():
    '''
    This function tests evaluating JavaScript code, and using it in Python program, expect success
    '''
    qjs = QuickJS()

    code = '''
        function f(x) {
            return 40 + x;
        }

        f
    '''

    with unwrap():
        qjs_func: _quickjs.Object = qjs.eval(code=code)
        result = qjs_func(10)
        assert result == 50


@pytest.mark.xfail(raises=JSException, strict=True)
def test_eval_exception():
    '''
    This function tests evaluating unpassable JavaScript code, expect exception
    '''
    qjs = QuickJS()
    code = '''unparsable js code'''

    with unwrap():
        qjs_func: _quickjs.Object = qjs.eval(code)

        
def test_get():
    '''
    This function tests evaluating JavaScript code with more than one function 
    and use it in Python program, expect success
    '''
    qjs = QuickJS()

    code = '''
        var f = (x) => {
            return 40 + x;
        };
        
        var f1 = (x, y) => {
            return x + y;
        };
    '''

    with unwrap():
        qjs.eval(code)
        func = qjs.get('f1')
        result = func(2, 3)
        assert result == 5


def test_get_as_json_true():
    '''
    This function tests evaluating JavaScript code,
    where value is json serializable, expect success
    '''
    qjs = QuickJS()

    code = '''
        var d = {
            'key': 10
        };

        d
    '''

    with unwrap():
        qjs.eval(code)
        d = qjs.get('d', as_json=True)
        assert isinstance(d, dict)
        assert d['key'] == 10


def test_get_returns_none():
    '''
    This function tests requiring non-existing variable from context, returns None
    '''
    qjs = QuickJS()

    with unwrap():
        func = qjs.get('f1')
        assert func is None


@pytest.mark.xfail(raises=TypeError, strict=True)
def test_get_exception():
    '''
    This function tests requiring None from context, expect exception
    '''
    qjs = QuickJS()

    with unwrap():
        func = qjs.get(None)


def test_set():
    '''
    This function tests changing variable value in context, expect success
    '''
    qjs = QuickJS()

    with unwrap():
        qjs.set('x', 8)
        qjs.set('x', 12)
        v = qjs.get('x')
        assert v == 12


@pytest.mark.xfail(raises=TypeError, strict=True)
def test_set_raises_exception():
    '''
    This function tests setting None as variable in context, expect exception
    '''
    qjs = QuickJS()

    with unwrap():
        qjs.set(None, '10')


def test_add_callable():
    '''
    This function tests adding Python callable in context, expect success
    '''
    qjs = QuickJS()

    with unwrap():
        py_name = 'pylam'
        py_func = lambda x: x * 10
        qjs.add_callable(py_name, py_func)

        f = qjs.get(py_name)
        result = f(5)
        assert result == 50


@pytest.mark.xfail(raises=TypeError, strict=True)
def test_add_callable_exception():
    '''
    This function tests adding invalid Python callable in context, expect exception
    '''
    qjs = QuickJS()

    with unwrap():
        py_name = 'pylam'
        f = qjs.add_callable(py_name, 'unparsable')


def test_set_memory_limit():
    '''
    This function tests setting memory limit in context, expect success
    '''
    qjs = QuickJS()
    qjs.set_memory_limit(memory_limit=1024000)

    with unwrap():
        qjs.set('x', 12)
        v = qjs.get('x')
        assert v == 12


def test_set_time_limit():
    '''
    This function tests setting time limit in context, expect success
    '''
    qjs = QuickJS()
    qjs.set_time_limit(time_limit=600)


def test_memory():
    '''
    This function tests check memory used size, expect success
    '''
    qjs = QuickJS()
    res: dict = qjs.memory()
    assert isinstance(res, dict)


def test_import_js_module_lodash():
    '''
    This function tests importing js module, expect success
    '''
    qjs = QuickJS()

    with unwrap():
        # import lodash
        path = os.path.join('/deps', 'vendor', 'lodash.js')
        qjs.import_js_module(path)

        # use lodash
        code = '''
            var a = _.range(10);

            a
        '''

        res = qjs.eval(code, as_json=True)
        expected: list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        assert res == expected


def test_import_js_module_automerge_0():
    '''
    This function tests importing automerge module, expect success
    '''
    qjs = QuickJS()

    with unwrap():
        # import encoding
        path = os.path.join('/deps',  'vendor', 'encoding.js')
        qjs.import_js_module(path)

        # import automerge
        path = os.path.join('/deps', 'vendor', 'automerge.js')
        qjs.import_js_module(path)

        # use automerge
        code = '''
            let card = {
                x: new Automerge.Counter(0),
                y: new Automerge.Counter(10),
                z: new Automerge.Counter(20),
            };

            let doc1s = Automerge.from({
                cards: [ card ]
            });

            // client A
            let doc1a = Automerge.change(Automerge.clone(doc1s), doc => {
                let card = { a: 0 };
                doc.cards.push(card);
            });

            // client B
            let doc1b = Automerge.change(Automerge.clone(doc1s), doc => {
                let card = { b: 10 };
                doc.cards.push(card);
            });

            // client C
            let doc1c = Automerge.change(Automerge.clone(doc1s), doc => {
                doc.cards[0].x.increment(100);
                doc.cards[0].y.increment(100);
                doc.cards[0].z.increment(100);
            });

            // client D
            let doc1d = Automerge.change(Automerge.clone(doc1s), doc => {
                doc.cards[0].x.increment(1000);
                doc.cards[0].y.increment(1000);
                doc.cards[0].z.increment(1000);
            });

            doc1s = Automerge.merge(Automerge.clone(doc1s), Automerge.clone(doc1b)); // B
            doc1s = Automerge.merge(Automerge.clone(doc1s), Automerge.clone(doc1a)); // A
            
            doc1s = Automerge.merge(Automerge.clone(doc1s), Automerge.clone(doc1d)); // D
            doc1s = Automerge.merge(Automerge.clone(doc1s), Automerge.clone(doc1c)); // C
            
            doc1s
        '''

        v: dict = qjs.eval(code, as_json=True)
        assert isinstance(v, dict)


def test_import_js_module_automerge_1():
    '''
    This function tests importing automerge module, expect success
    '''
    qjs = QuickJS()

    with unwrap():
        # import encoding
        path = os.path.join('/deps', 'vendor', 'encoding.js')
        qjs.import_js_module(path)

        # import automerge
        path = os.path.join('/deps', 'vendor', 'automerge.js')
        qjs.import_js_module(path)

        # use automerge
        code = '''
            var doc1s = Automerge.from({
                cards: [{}]
            });

            // client A
            let doc1a = Automerge.change(Automerge.clone(doc1s), doc => {
                doc.cards[0].x = 0;
                doc.cards[0].y = 10;
            });

            let doc1aChanges = Automerge.getChanges(doc1s, doc1a);

            // client B
            let doc1b = Automerge.change(Automerge.clone(doc1s), doc => {
                doc.cards[0].y = 20;
                doc.cards[0].z = 30;
            });

            let doc1bChanges = Automerge.getChanges(doc1s, doc1b);

            // apply changes
            var [doc1s, _] = Automerge.applyChanges(doc1s, doc1aChanges); // A
            var [doc1s, _] = Automerge.applyChanges(doc1s, doc1bChanges); // B

            // client C
            let doc1c = Automerge.change(Automerge.clone(doc1s), doc => {
                doc.cards[0].x = 100;
                doc.cards[0].y = 200;
                doc.cards[0].z = 300;
            });

            let doc1cChanges = Automerge.getChanges(doc1s, doc1c);

            // apply changes
            var [doc1s, _] = Automerge.applyChanges(doc1s, doc1cChanges); // C

            doc1s
        '''

        v: dict = qjs.eval(code, as_json=True)
        assert isinstance(v, dict)


def test_get_function():
    qjs = QuickJS()

    with unwrap():
        # call JS function
        code = '''
            var f = (x) => {
                return [ x ];
            };
        '''

        qjs.eval(code)
        f: callable = qjs.get_function('f')
        result = f(10)
        assert result == [10]


def test_eval_python_js_functions_bidirectionally():
    qjs = QuickJS()

    with unwrap():
        # call JS function
        code = '''
            function f(x) {
                return 40 + x;
            }

            f
        '''

        f: callable = qjs.eval(code)
        result = f(10)
        assert result == 50

        # register Python function inside JS runtime
        py_name = 'pylam'
        py_func = lambda x: x * 10
        qjs.add_callable(py_name, py_func)

        # call Python function from JS runtime
        code = '''
            function g(x) {
                return pylam(x);
            }

            g
        '''

        g: callable = qjs.eval(code)
        result = g(10)
        assert result == 100
        
        # call Python function registered in JS runtime
        pylam = qjs.get(py_name)
        result = pylam(5)
        assert result == 50


def test_eval_python_js_functions_bidirectionally_json():
    qjs = QuickJS()

    with unwrap():
        # call JS function
        code = '''
            var f = (x) => {
                return [ x ];
            };
        '''

        qjs.eval(code)
        f: callable = qjs.get_function('f')
        result = f(10)
        assert result == [10]

        # register Python function inside JS runtime
        py_name = 'pylam'
        py_func = lambda x: json.dumps([x * 10])
        qjs.add_callable(py_name, py_func)

        # call Python function from JS runtime
        code = '''
            var g = (x) => {
                return [ JSON.parse(pylam(x)) ];
            };
        '''

        qjs.eval(code)
        g: callable = qjs.get_function('g')
        result = g(10)
        assert result == [[100]]
        
        # call Python function registered in JS runtime
        pylam = qjs.get(py_name)
        result = pylam(5)
        assert result == json.dumps([50])


"""
def test_import_es_module():
    qjs = QuickJS()

    # import/use luxon
    code = '''
        import { DateTime } from '/deps/thquickjs/vendor/luxon.js';
        DateTime.now().toString();
    '''

    match qjs.eval(code, as_json=True):
        case Ok(v):
            print(f'v[1]: {type(v)!r} {v!r}')
        case Err(e):
            raise Exception(e)
"""
