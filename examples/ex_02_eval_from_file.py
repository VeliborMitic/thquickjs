from thquickjs import QuickJS


# create instance of QuickJS
qjs = QuickJS()

# js code can be loaded from file, file extension doesn't need to be js
file_name  = 'abc.txt'

code = '''

    function m(n) {
        return n * 10;
    }

    m

'''

# write js code in file
with open(file_name, 'w') as f:
    f.write(code)

# read js code from file
with open(file_name, 'r') as reader:
    content = reader.read()

# evaluate js code, and return python code, pf is evaluated function m from js code
# pf is actually _quickjs.Object, but it can be invoked as python function
pf = qjs.eval(content).unwrap()

# call function pf, or function m from js code
pf(20) # 200: 20 * 10
