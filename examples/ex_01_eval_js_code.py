from thquickjs import QuickJS


# create instance of QuickJS
qjs = QuickJS()

# string contains js code
js_code = '''

    function f(n) {
        return n + 10;
    }

    f

'''

# evaluate js code, and return python code, py_func is evaluated function f from js code
# py_func is actually _quickjs.Object, but it can be invoked as python function
py_func = qjs.eval(js_code).unwrap()

# call function py_func, or function f from js code
py_func(30) # returns 40: 30 + 10
