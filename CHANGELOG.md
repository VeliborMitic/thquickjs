# Changelog

## 0.9.26
- Fixed: Simple and Advanced `README.md` example.

## 0.9.25
- Fixed: Better `README.md` example.

## 0.9.24
- Added: Coverage badge.
- Added: Build badge.
- Added: `CHANGELOG.md`.
- Added: `EXAMPLES.md`.
- Fixed: `README.md` documentation.
- Fixed: CI/CD, `coverage` and `test` jobs.
- Fixed: removed `auto_unwrap` test and code.
- Fixed: Modernized tests using new version of `thresult`.
